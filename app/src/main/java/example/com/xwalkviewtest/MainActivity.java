package example.com.xwalkviewtest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import java.util.HashMap;

public class MainActivity extends AppCompatActivity {

    private final HashMap<Integer, String> pageTitles = new HashMap<>();
    private final HashMap<Integer, Integer> pageProgresses = new HashMap<>();
    private int currentTabPosition;
    private AnimatingProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.main_toolbar);
        setSupportActionBar(toolbar);

        final ViewPager viewPager = (ViewPager) findViewById(R.id.main_viewpager);
        viewPager.setOffscreenPageLimit(5);
        viewPager.setAdapter(new MyPagerAdapter(getSupportFragmentManager()));

        progressBar = (AnimatingProgressBar) findViewById(R.id.main_progress_bar);
        progressBar.setMax(100);

        final TabLayout tabLayout = (TabLayout) findViewById(R.id.main_tab_layout);
        currentTabPosition = 0;
        tabLayout.addTab(tabLayout.newTab().setText("Tab 1"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 2"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 3"));
        tabLayout.addTab(tabLayout.newTab().setText("Tab 4"));
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                final int position = tab.getPosition();
                if (currentTabPosition != position) {
                    setTitle(pageTitles.get(position));
                    currentTabPosition = position;
                    final Integer progress = pageProgresses.get(position);
                    if (progress != null) {
                        updateProgress(progress);
                    }
                    viewPager.setCurrentItem(tab.getPosition());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    public void updateTitle(final int position, final String newTitle) {
        pageTitles.put(position, newTitle);
        if (position == currentTabPosition) {
            setTitle(newTitle);
        }
    }

    public void updateProgressBar(final int position, final int progress) {
        pageProgresses.put(position, progress);
        if (position == currentTabPosition) {
            updateProgress(progress);
        }
    }

    private void updateProgress(final int progress) {
        if (progressBar.getAlpha() != 1f && progress != 100 && progress != 0) {
            showProgressBar(true);
        }
        progressBar.setProgress(progress);
        if (progress == 100) {
            progressBar.setProgress(0);
            showProgressBar(false);
        }
    }

    private void showProgressBar(final boolean show) {
        progressBar.animate().alpha(show ? 1f : 0f).setDuration(show ? 0 : 200).start();
    }

    private class MyPagerAdapter extends FragmentPagerAdapter {

        public MyPagerAdapter(@NonNull final FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int pos) {
            switch (pos) {
                case 0:
                    return CustomUrlFragment.newInstance("http://www.drive2.ru/", pos);
                case 1:
                    return CustomUrlFragment.newInstance("http://www.drive2.ru/my/feed/", pos);
                case 2:
                    return CustomUrlFragment.newInstance("http://www.drive2.ru/my/notifications/", pos);
                case 3:
                    return CustomUrlFragment.newInstance("http://www.drive2.ru/my/", pos);
                default:
                    return CustomUrlFragment.newInstance("ThirdFragment, Default", pos);
            }
        }

        @Override
        public int getCount() {
            return 4;
        }

    }

}
