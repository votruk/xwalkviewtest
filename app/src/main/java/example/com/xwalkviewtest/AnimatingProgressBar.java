package example.com.xwalkviewtest;

import android.animation.ValueAnimator;
import android.content.Context;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.ProgressBar;

public class AnimatingProgressBar extends ProgressBar {

    private static final Interpolator DEFAULT_INTERPOLATOR = new LinearInterpolator();

    @Nullable
    private ValueAnimator animator;
    private boolean animate = true;

    public AnimatingProgressBar(final Context context, final AttributeSet attrs, final int defStyle) {
        super(context, attrs, defStyle);
    }

    public AnimatingProgressBar(final Context context, final AttributeSet attrs) {
        super(context, attrs);
    }

    public AnimatingProgressBar(final Context context) {
        super(context);
    }

    public boolean isAnimate() {
        return animate;
    }

    public void setAnimate(final boolean animate) {
        this.animate = animate;
    }

    @Override
    @SuppressWarnings({"PMD.AvoidSynchronizedAtMethodLevel", "PMD.UselessQualifiedThis"}) // Overriding ProgresBbar method
    public synchronized void setProgress(final int progress) {
        if (!animate) {
            super.setProgress(progress);
            return;
        }
        if (animator != null) {
            animator.cancel();
        }
        if (animator == null) {
            animator = ValueAnimator.ofInt(getProgress(), progress);
            animator.setInterpolator(DEFAULT_INTERPOLATOR);
            animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                @Override
                public void onAnimationUpdate(final ValueAnimator animation) {
                    AnimatingProgressBar.super.setProgress((Integer) animation.getAnimatedValue());
                }
            });
        } else {
            animator.setIntValues(getProgress(), progress);
        }
        animator.start();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (animator != null) {
            animator.cancel();
        }
    }

}