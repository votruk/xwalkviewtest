package example.com.xwalkviewtest;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class StartupActivity extends AppCompatActivity {

    private View goButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup);
        final Toolbar toolbar = (Toolbar) findViewById(R.id.startup_toolbar);
        setSupportActionBar(toolbar);
        goButton = findViewById(R.id.startup_toolbar_button);
        getSupportFragmentManager()
                .beginTransaction()
                .add(R.id.startup_content, CustomUrlFragment.newInstance("http://www.drive2.ru/my/", 0))
                .commit();
    }

    @NonNull
    public View getGoButton() {
        return goButton;
    }

    //
//    @Override
//    public boolean onCreateOptionsMenu(Menu menu) {
//        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.menu_startup, menu);
//        return true;
//    }

//    @Override
//    public boolean onOptionsItemSelected(MenuItem item) {
//        // Handle action bar item clicks here. The action bar will
//        // automatically handle clicks on the Home/Up button, so long
//        // as you specify a parent activity in AndroidManifest.xml.
//        int id = item.getItemId();
//
//        //noinspection SimplifiableIfStatement
//        if (id == R.id.menu_ok) {
//            startActivity(new Intent(this, MainActivity.class));
//            finish();
//            return true;
//        }
//
//        return super.onOptionsItemSelected(item);
//    }

}
