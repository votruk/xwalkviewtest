package example.com.xwalkviewtest;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.xwalk.core.XWalkNavigationHistory;
import org.xwalk.core.XWalkPreferences;
import org.xwalk.core.XWalkResourceClient;
import org.xwalk.core.XWalkUIClient;
import org.xwalk.core.XWalkView;

public class CustomUrlFragment extends Fragment {

    private static final String URL_EXTRA = "URL_EXTRA";
    private static final String POSITION_EXTRA = "POSITION_EXTRA";
    private SwipeRefreshLayout webviewRefresher;

    private boolean loading;
    private XWalkView xWalkView;
//    private View progressBar;

    private int position;

    @NonNull
    public static CustomUrlFragment newInstance(@NonNull final String url, final int position) {
        final CustomUrlFragment fragment = new CustomUrlFragment();
        Bundle bundle = new Bundle();
        bundle.putString(URL_EXTRA, url);
        bundle.putInt(POSITION_EXTRA, position);

        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View onCreateView(final LayoutInflater inflater, final ViewGroup container, final Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_custom_url, container, false);

        xWalkView = (XWalkView) view.findViewById(R.id.custom_url_xwalkview);
        xWalkView.setUIClient(getUiClient(xWalkView));
        xWalkView.setResourceClient(getResourceClient(xWalkView));
        xWalkView.load(getArguments().getString(URL_EXTRA), null);
        position = getArguments().getInt(POSITION_EXTRA);
        xWalkView.setUserAgentString("drive2client/3.2 (Android 6.0;Smartphone;unknown Android SDK built for x86)");
        XWalkPreferences.setValue(XWalkPreferences.REMOTE_DEBUGGING, true);
        webviewRefresher = (SwipeRefreshLayout) view.findViewById(R.id.custom_url_screen_refresher);
        webviewRefresher.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (!loading) {
                    xWalkView.reload(XWalkView.RELOAD_NORMAL);
                }
            }
        });

        if (getActivity() instanceof StartupActivity) {
            final StartupActivity activity = (StartupActivity) getActivity();
            activity.getGoButton().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View v) {
                    xWalkView.load("https://www.drive2.ru/my/feed/", null);
                }
            });
        }

//        progressBar = view.findViewById(R.id.custom_url_progress_bar);

        return view;
    }

    @NonNull
    private XWalkResourceClient getResourceClient(@NonNull final XWalkView xWalkView) {
        return new XWalkResourceClient(xWalkView) {

            @Override
            public void onDocumentLoadedInFrame(final XWalkView view, final long frameId) {
//                progressBar.setVisibility(View.GONE);
//                xWalkView.setVisibility(View.VISIBLE);
                Log.d("XWalkResourceClient", "onDocumentLoadedInFrame");
                super.onDocumentLoadedInFrame(view, frameId);
            }

            @Override
            public boolean shouldOverrideUrlLoading(final XWalkView view, final String url) {
                Log.d("XWalkResourceClient", "shouldOverrideUrlLoading");
                return super.shouldOverrideUrlLoading(view, url);
            }

            @Override
            public void onProgressChanged(final XWalkView view, final int progressInPercent) {
                super.onProgressChanged(view, progressInPercent);
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity)getActivity()).updateProgressBar(position, progressInPercent);
                }
            }
        };
    }

    @NonNull
    private XWalkUIClient getUiClient(@NonNull final XWalkView xWalkView) {
        return new XWalkUIClient(xWalkView) {

            @Override
            public void onPageLoadStarted(final XWalkView view, final String url) {
//                progressBar.setVisibility(View.VISIBLE);
//                webviewRefresher.setVisibility(View.GONE);
                loading = true;
                final XWalkNavigationHistory history = xWalkView.getNavigationHistory();
                if (history.getCurrentItem() != null) {
                    if (!TextUtils.isEmpty(history.getCurrentItem().getTitle())) {
                        if (getActivity() instanceof MainActivity) {
                            ((MainActivity)getActivity()).updateTitle(position, history.getCurrentItem().getTitle());
                        }
                    }
                }
                Log.d("XWalkUIClient", "onPageLoadStarted");
                super.onPageLoadStarted(view, url);
            }

            @Override
            public void onPageLoadStopped(final XWalkView view, final String url, final LoadStatus status) {
                super.onPageLoadStopped(view, url, status);
//                progressBar.setVisibility(View.GONE);
//                webviewRefresher.setVisibility(View.VISIBLE);
                webviewRefresher.setRefreshing(false);
                loading = false;
            }

            @Override
            public void onReceivedTitle(final XWalkView view, final String title) {
                Log.d("XWalkUIClient", "onReceivedTitle");
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity)getActivity()).updateTitle(position, title);
                }
                super.onReceivedTitle(view, title);
            }

        };
    }

}
